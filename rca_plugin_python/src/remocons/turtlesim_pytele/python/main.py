# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.
import time
import math

import rosgraph
import rospy

from rcapy import RcaPyException
import std_srvs.srv
import rcapy_impl
import turtlesim.srv


class Impl(rcapy_impl.APyRemoConImplBase):
    BUTTON_SWITCH_JOYPAD = "button_switch_joypad"
    TURTLE_CHOOSER = "turtle_chooser"

    NODE_NAME_TURTLESIM = "turtlesim_node"

    NSID_TURTLESIM = "turtlesim"
    NSID_TURTLE = "turtle"

    REMOCOMP_JOYPAD = "joypad"
    REMOCOMP_TURTLESIM_COMMANDS = "turtlesim_commands"
    REMOCOMP_TURTLE_COMMANDS = "turtle_commands"
    REMOCOMP_TURTLE_SCREEN = "turtle_screen"

    CMD_TURTLESIM_SET_TURTLESIM = "set_turtlesim"
    CMD_TURTLESIM_UPDATE = "update"
    CMD_TURTLESIM_CLEAR = "clear"
    CMD_TURTLESIM_RESET = "reset"
    CMD_TURTLESIM_SPAWN = "spawn"
    CMD_TURTLE_SET_PEN = "set_pen"
    CMD_TURTLE_TELEPORT_ABSOLUTE = "teleport_absolute"
    CMD_TURTLE_TELEPORT_RELATIVE = "teleport_relative"
    CMD_TURTLE_KILL = "kill"

    RAD = math.pi / 180

    def __init__(self, context):
        """
        @type context: APyRemoConContext
        """
        super(Impl, self).__init__(context)
        self.droid = self.context.create_android_proxy()

        self.state = self.context.get_remocon_session_state()
        if self.state is None:
            self.state = dict()
            self.state["selected_turtle"] = None
            self.state["pen_color"] = 0xffb3b8ff
            self.state["pen_width"] = 3
            self.state["pen_off"] = False
            self.state["tel_rel_linear"] = 2.0
            self.state["tel_rel_angular"] = 144.0
            self.context.set_remocon_session_state(self.state)

        self.commands = dict()
        self.turtlesim_node_name = None
        self.xml_resources = dict()

        def set_namespace():
            turtlesim_nodes = []
            state = self.get_ros_system_state()
            if state is None:
                return
            pubs, _, _ = state
            for _, pub in pubs:
                turtlesim_nodes.extend([rospy.resolve_name(n)
                                        for n in pub if self.get_graph_basename(n) == self.NSID_TURTLESIM])
            turtlesim_nodes = list(set(turtlesim_nodes))

            droid = self.context.create_android_proxy()
            droid.sl4a.dialogCreateAlert("Choose Turtlesim", None)
            droid.sl4a.dialogSetItems(turtlesim_nodes)
            droid.sl4a.dialogShow()
            response = droid.sl4a.dialogGetResponse()
            droid.sl4a.dialogDismiss()
            droid.shutdown()

            if "item" in response:
                self.relauncher.set_namespace(self.NSID_TURTLESIM,
                                              self.get_graph_parent(turtlesim_nodes[response["item"]])).commit()

        self.commands[self.CMD_TURTLESIM_SET_TURTLESIM] = set_namespace

        def update_turtle_list():
            self.update_turtle_chooser()

        self.commands[self.CMD_TURTLESIM_UPDATE] = update_turtle_list

        def call_service(name, srv_type, call):
            """
            @type name: str
            @type srv_type: object
            @type call: (genpy.Message) -> None
            @rtype: bool
            """
            # noinspection PyBroadException
            try:
                request = rospy.ServiceProxy(name, srv_type)
                """@type: genpy.Message"""
                call(request)
                self.log.i("success calling service " + name)
                return True
            except:
                self.log.e("failure calling service " + name)
                return False

        def spawn():
            def handle_event(proxy, name, data):
                """
                @type proxy: rcapy.AAndroidProxy
                @type name: str
                @type data: dict[str, object]
                """
                apply_action, dismiss = self.handle_dialog_event(name, data)

                if apply_action:
                    def get_value(view_id, default_value):
                        value = proxy.sl4a.fullQueryDetail(view_id)["text"]
                        if value is None or value == "":
                            return default_value
                        return value

                    turtle_name = str(get_value("name", ""))
                    x = float(get_value("x", 5))
                    y = float(get_value("y", 5))
                    theta = self.RAD * float(get_value("theta", 0))

                    def call(request):
                        """
                        @type request: SpawnRequest
                        """
                        # noinspection PyCallingNonCallable
                        response = request(name=turtle_name, x=x, y=y, theta=theta)
                        """@type: SpawnResponse"""
                        self.update_turtle_chooser(response.name)

                    call_service(self.CMD_TURTLESIM_SPAWN, turtlesim.srv.Spawn, call)

                if dismiss:
                    proxy.sl4a.fullDismiss()
                    proxy.shutdown()

            droid = self.context.create_android_proxy()
            droid.set_listener(handle_event)
            droid.sl4a.fullShow(self.read_xml_resource("turtle_spawn_dialog"), "Spawn Turtle")
            # Note: The proxy will be shutdown in the event listener.

        self.commands[self.CMD_TURTLESIM_SPAWN] = spawn

        def clear():
            def handle_event(proxy, name, data):
                """
                @type proxy: rcapy.AAndroidProxy
                @type name: str
                @type data: dict[str, object]
                """
                apply_action, dismiss = self.handle_dialog_event(name, data)

                if apply_action:
                    def call(request):
                        """
                        @type request: EmptyRequest
                        """
                        # noinspection PyCallingNonCallable
                        request()

                    self.set_background_params(proxy.sl4a.fullGetProperty("color", "currentColor"))
                    call_service(self.CMD_TURTLESIM_CLEAR, std_srvs.srv.Empty, call)

                if dismiss:
                    proxy.sl4a.fullDismiss()
                    proxy.shutdown()

            droid = self.context.create_android_proxy()
            droid.set_listener(handle_event)

            # Note: Convert to hex string (starting with 0x) for correctly recognizing the type by SL4A.
            current_color = "0x%x" % self.get_background_params()
            self.log.w("color=" + current_color)
            layout = self.read_xml_resource("turtlesim_clear_dialog") % (current_color, current_color)
            self.log.w("layout=" + layout)
            droid.sl4a.fullShow(layout, "Clear")

            # Another approach (only applicable after inflation, i.e. after calling fullShow):
            # droid.sl4a.fullSetProperty("color", "currentColor", current_color)
            # droid.sl4a.fullSetProperty("color", "defaultColor", current_color)

            # Note: The proxy will be shutdown in the event listener.

        self.commands[self.CMD_TURTLESIM_CLEAR] = clear

        def reset():
            def call(request):
                """
                @type request: EmptyRequest
                """
                # noinspection PyCallingNonCallable
                request()
                # The turtlesim node will disconnect all subscribed topics, so we won't get any messages anymore. As a
                # workaround, reconnect all topics by forcing a remocon relaunch.
                self.relauncher.set_param("relaunch_force", time.time())
                self.update_turtle_chooser()

            call_service(self.CMD_TURTLESIM_RESET, std_srvs.srv.Empty, call)

        self.commands[self.CMD_TURTLESIM_RESET] = reset

        def set_pen():
            turtle_ns = self.get_turtle_ns()
            if turtle_ns is None:
                return

            def handle_event(proxy, name, data):
                """
                @type proxy: rcapy.AAndroidProxy
                @type name: str
                @type data: dict[str, object]
                """
                apply_action, dismiss = self.handle_dialog_event(name, data)

                if apply_action:
                    self.state["pen_color"] = color = proxy.sl4a.fullGetProperty("color", "currentColor")
                    r = color >> 16 & 0xff
                    g = color >> 8 & 0xff
                    b = color & 0xff
                    self.state["pen_width"] = width = int(proxy.sl4a.fullGetProperty("width", "progress"))
                    if proxy.sl4a.fullGetProperty("off", "checked"):
                        off = 1
                    else:
                        off = 0
                    self.state["pen_off"] = off != 0

                    def call(request):
                        """
                        @type request: SetPenRequest
                        """
                        # noinspection PyCallingNonCallable
                        request(r=r, g=g, b=b, width=width, off=off)

                    call_service(rosgraph.names.ns_join(turtle_ns, self.CMD_TURTLE_SET_PEN), turtlesim.srv.SetPen, call)

                if dismiss:
                    proxy.sl4a.fullDismiss()
                    proxy.shutdown()

            droid = self.context.create_android_proxy()
            droid.set_listener(handle_event)

            # Note: Convert to hex string (starting with 0x) for correctly recognizing the type by SL4A.
            layout = self.read_xml_resource("turtle_set_pen_dialog") % (
                self.state["pen_color"], self.state["pen_color"], self.state["pen_width"], self.state["pen_off"])
            droid.sl4a.fullShow(layout, "Clear")

            # Note: The proxy will be shutdown in the event listener.

        self.commands[self.CMD_TURTLE_SET_PEN] = set_pen

        def teleport_absolute():
            turtle_ns = self.get_turtle_ns()
            if turtle_ns is None:
                return

            def handle_event(proxy, name, data):
                """
                @type proxy: rcapy.AAndroidProxy
                @type name: str
                @type data: dict[str, object]
                """
                apply_action, dismiss = self.handle_dialog_event(name, data)

                if apply_action:
                    def get_value(view_id, default_value):
                        value = proxy.sl4a.fullQueryDetail(view_id)["text"]
                        if value is None or value == "":
                            return default_value
                        return value

                    x = float(get_value("x", 5))
                    y = float(get_value("y", 5))
                    theta = math.pi / 180 * float(get_value("theta", 0))

                    def call(request):
                        """
                        @type request: TeleportAbsoluteRequest
                        """
                        # noinspection PyCallingNonCallable
                        request(x=x, y=y, theta=theta)

                    call_service(rosgraph.names.ns_join(turtle_ns, self.CMD_TURTLE_TELEPORT_ABSOLUTE),
                                 turtlesim.srv.TeleportAbsolute,
                                 call)

                if dismiss:
                    proxy.sl4a.fullDismiss()
                    proxy.shutdown()

            droid = self.context.create_android_proxy()
            droid.set_listener(handle_event)
            droid.sl4a.fullShow(self.read_xml_resource("turtle_teleport_absolute_dialog"), "Teleport Turtle Absolute")
            # Note: The proxy will be shutdown in the event listener.

        self.commands[self.CMD_TURTLE_TELEPORT_ABSOLUTE] = teleport_absolute

        def teleport_relative():
            turtle_ns = self.get_turtle_ns()
            if turtle_ns is None:
                return

            def handle_event(proxy, name, data):
                """
                @type proxy: rcapy.AAndroidProxy
                @type name: str
                @type data: dict[str, object]
                """
                apply_action, dismiss = self.handle_dialog_event(name, data)

                if apply_action:
                    def get_value(view_id, default_value):
                        value = proxy.sl4a.fullQueryDetail(view_id)["text"]
                        if value is None or value == "":
                            return default_value
                        return value

                    self.state["tel_rel_linear"] = linear = float(get_value("linear", 5))
                    self.state["tel_rel_angular"] = angular = float(get_value("angular", 0))

                    def call(request):
                        """
                        @type request: TeleportRelativeRequest
                        """
                        # noinspection PyCallingNonCallable
                        request(linear=linear, angular=self.RAD * angular)

                    call_service(rosgraph.names.ns_join(turtle_ns, self.CMD_TURTLE_TELEPORT_RELATIVE),
                                 turtlesim.srv.TeleportRelative,
                                 call)
                elif dismiss:
                    proxy.sl4a.fullDismiss()
                    proxy.shutdown()

            droid = self.context.create_android_proxy()
            droid.set_listener(handle_event)
            layout = self.read_xml_resource("turtle_teleport_relative_dialog") % (
                self.state["tel_rel_linear"], self.state["tel_rel_angular"])
            droid.sl4a.fullShow(layout, "Teleport Turtle Relative")
            # Note: The proxy will be shutdown in the event listener.

        self.commands[self.CMD_TURTLE_TELEPORT_RELATIVE] = teleport_relative

        def kill():
            turtle = self.get_selected_turtle()
            if turtle is None:
                return

            def call(request):
                """
                @type request: KillRequest
                """
                # noinspection PyCallingNonCallable
                request(name=turtle)
                self.update_turtle_chooser()

            call_service(self.CMD_TURTLE_KILL, turtlesim.srv.Kill, call)

        self.commands[self.CMD_TURTLE_KILL] = kill

    def start(self, configuration):
        rospy.init_node(*configuration.create_node_configuration(self.context.the_type))
        self.turtlesim_node_name = rospy.resolve_name(self.NODE_NAME_TURTLESIM)

        self.context.get_remocomp_proxy(self.REMOCOMP_JOYPAD).set_on_launched_listener(self.on_remocomp_launched_event)
        self.context.get_remocomp_proxy(self.REMOCOMP_TURTLESIM_COMMANDS) \
            .set_events_listener(self.handle_remocomp_event)
        self.context.get_remocomp_proxy(self.REMOCOMP_TURTLE_COMMANDS) \
            .set_events_listener(self.handle_remocomp_event)
        self.context.get_remocomp_proxy(self.REMOCOMP_TURTLE_SCREEN) \
            .set_events_listener(self.handle_remocomp_event)

        self.update_turtle_chooser(self.state["selected_turtle"])
        self.parent_view.set_on_click_listener(self.BUTTON_SWITCH_JOYPAD, self.on_click_switch_joypad)
        self.parent_view.set_on_custom_event_listener(self.TURTLE_CHOOSER, self.on_custom_event_turtle_chooser)

    def shutdown(self):
        self.state["selected_turtle"] = self.get_selected_turtle()

    def on_click_switch_joypad(self, name, data):
        """
        @type name: str
        @type data: dict[str, object]
        """
        assert name == "click"
        assert data["id"] == self.BUTTON_SWITCH_JOYPAD

        proxy = self.context.get_remocomp_proxy(self.REMOCOMP_JOYPAD)
        if proxy.the_type == "simple_joypad":
            self.relauncher.set_type(self.REMOCOMP_JOYPAD, "ros_virtual_joystick", "com.github.rosjava.rca_plugin_std")
        else:
            self.relauncher.set_type(self.REMOCOMP_JOYPAD, "simple_joypad", "com.github.rosjava.rca_plugin_std")

        self.relauncher.commit()

    def on_custom_event_turtle_chooser(self, name, data):
        """
        @type name: str
        @type data: dict[str, object]
        """
        assert name == "custom"
        assert data["id"] == self.TURTLE_CHOOSER
        custom = data["custom"]
        custom_name = custom["name"]
        """@type: str"""

        if custom_name == "turtle_selected":
            self.enable_turtle_ui()
        elif custom_name == "nothing_selected":
            self.disable_turtle_ui()

    def on_remocomp_launched_event(self, proxy):
        """
        @type proxy: rcapy.ARemoCompProxy
        """
        if proxy.the_type == "ros_virtual_joystick":
            self.layout.parent_view.set_property(self.REMOCOMP_JOYPAD, "layout_width", "300dip")
            self.layout.parent_view.call_method_on_ui_thread(self.REMOCOMP_JOYPAD, "requestLayout", ())

    # noinspection PyUnusedLocal
    def handle_remocomp_event(self, proxy, name, data):
        """
        @type proxy: rcapy.ARemoCompProxy
        @type name: str
        @type data: dict[str, object]
        """
        if name == "hit_the_wall":
            self.droid.sl4a.vibrate(100)
        elif name == "buttons_container_event":
            self.call_command(data["message"])
        else:
            self.log.w("ignoring unknown remocomp event: '%s'" % name)

    def update_turtle_chooser(self, select_turtle=None):
        """
        @type select_turtle: str | None
        """
        turtle_topics = list()
        turtle_topics.append("cmd_vel")
        turtle_topics.append("color_sensor")
        turtle_topics.append("pose")

        turtle_candidates = dict()
        topics = set()

        state = self.get_ros_system_state()
        if state is not None:
            pubs, subs, _ = state
            for topic in pubs:
                if self.turtlesim_node_name in topic[1]:
                    topics.add(topic[0])
            for topic in subs:
                if self.turtlesim_node_name in topic[1]:
                    topics.add(topic[0])

        turtlesim_ns = self.get_graph_parent(self.turtlesim_node_name)
        for topic in topics:
            if self.get_graph_basename(topic) in turtle_topics and \
                    self.get_graph_parent(self.get_graph_parent(topic)) == turtlesim_ns:
                turtle = self.get_graph_parent(topic)
                matched_topics_count = turtle_candidates.get(turtle, 0)
                turtle_candidates[turtle] = matched_topics_count + 1

        turtles = list()
        for turtle in turtle_candidates.keys():
            if turtle_candidates[turtle] == len(turtle_topics):
                turtles.append(self.get_graph_basename(turtle))

        if len(turtles) == 0:
            self.disable_turtle_ui()
        self.parent_view.call_method(self.TURTLE_CHOOSER, "setTurtles", (turtles, select_turtle))

    def enable_turtle_ui(self):
        self.parent_view.set_property(self.TURTLE_CHOOSER, "enabled", "true")
        self.context.get_remocomp_proxy(self.REMOCOMP_TURTLE_COMMANDS).binder.setAllButtonsEnabled(True)
        self.context.get_remocomp_proxy(self.REMOCOMP_TURTLESIM_COMMANDS).binder.setAllButtonsEnabled(True)
        self.relauncher.set_namespace("turtle", self.get_turtle_ns()).commit()

    def disable_turtle_ui(self):
        self.parent_view.set_property(self.TURTLE_CHOOSER, "enabled", "false")
        self.context.get_remocomp_proxy(self.REMOCOMP_TURTLE_COMMANDS).binder.setAllButtonsEnabled(False)
        binder = self.context.get_remocomp_proxy(self.REMOCOMP_TURTLESIM_COMMANDS).binder
        binder.setAllButtonsEnabled(False)
        binder.setButtonEnabled(self.CMD_TURTLESIM_SET_TURTLESIM, True)
        binder.setButtonEnabled(self.CMD_TURTLESIM_UPDATE, True)
        binder.setButtonEnabled(self.CMD_TURTLESIM_SPAWN, True)

        self.relauncher.set_namespace(self.NSID_TURTLE, "").commit()

    def call_command(self, cmd_name):
        """
        @type cmd_name: str
        """
        command = self.commands.get(cmd_name, None)
        if command is not None:
            command()
        else:
            self.log.w("ignoring unknown command: '%s'" % cmd_name)

    @staticmethod
    def handle_dialog_event(name, data):
        """
        @type name: str
        @type data: dict[str, object]
        @rtype: tuple[bool, bool]
        """
        apply_action = False
        dismiss = False
        if name == "click":
            view_id = data["id"]
            if view_id == "cancel":
                dismiss = True
            elif view_id == "apply":
                dismiss = True
                apply_action = True
        elif name == "screen":
            if data == "destroy":
                dismiss = True
        elif name == "key":
            if data["key"] == "4":
                dismiss = True
        return apply_action, dismiss

    def get_turtle_ns(self):
        selected_turtle = self.get_selected_turtle()
        if selected_turtle is None:
            return None
        return rosgraph.names.ns_join(self.get_graph_parent(self.turtlesim_node_name), selected_turtle)

    @staticmethod
    def set_background_params(color):
        """
        @type color: int
        """
        rospy.set_param("background_r", (color >> 16) & 0xff)
        rospy.set_param("background_g", (color >> 8) & 0xff)
        rospy.set_param("background_b", color & 0xff)

    def get_background_params(self):
        """
        @rtype int
        """
        r = rospy.get_param("background_r", 0)
        g = rospy.get_param("background_g", 0)
        b = rospy.get_param("background_b", 0)
        self.log.w(rospy.resolve_name("background_r"))
        self.log.w("r=" + str(r))
        self.log.w("g=" + str(g))
        self.log.w("b=" + str(b))
        return (0xff << 24) | r << 16 | g << 8 | b

    def get_selected_turtle(self):
        """
        @rtype: str
        """
        return self.parent_view.get_property(self.TURTLE_CHOOSER, "selectedTurtle")

    @staticmethod
    def get_graph_basename(graph_name):
        """
        @type graph_name: str
        @rtype str
        """
        return graph_name[graph_name.rfind("/") + 1:]

    @staticmethod
    def get_graph_parent(graph_name):
        """
        @type graph_name: str
        @rtype str
        """
        parent = graph_name[:graph_name.rfind("/")]
        if parent == "":
            return "/"
        return parent

    def get_ros_system_state(self):
        """
        Retrieve list representation of system state (i.e. publishers, subscribers, and services).
        @rtype: [[str,[str]], [str,[str]], [str,[str]]]
        @return: systemState

           System state is in list representation::
             [publishers, subscribers, services].

           publishers is of the form::
             [ [topic1, [topic1Publisher1...topic1PublisherN]] ... ]

           subscribers is of the form::
             [ [topic1, [topic1Subscriber1...topic1SubscriberN]] ... ]

           services is of the form::
             [ [service1, [service1Provider1...service1ProviderN]] ... ]
        """
        master = rosgraph.Master(self.context.namespace)
        try:
            return master.getSystemState()
        except Exception as e:
            raise RcaPyException("Could not retrieve ROS system state.", e)

    def read_xml_resource(self, resource):
        """
        @type resource: str
        @rtype: str
        """
        xml_string = self.xml_resources.get(resource)
        if xml_string is None:
            with open("../res/layout/%s.xml" % resource) as layout_file:
                xml_string = layout_file.read()
            self.xml_resources[resource] = xml_string
        return xml_string

