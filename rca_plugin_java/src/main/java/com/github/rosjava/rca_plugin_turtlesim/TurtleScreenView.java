/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.rca_plugin_turtlesim;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

public class TurtleScreenView extends TextView {
	private Paint mPaint = null;
	private float mMinPos = 0f;
	private float mMaxPos = 11.08f;
	private float mTurtleX = Float.NaN;
	private float mTurtleY = Float.NaN;
	private float mTheta = Float.NaN;
	private int mSensorColor = 0;

	public TurtleScreenView(Context context) {
		super(context);
		init(context);
	}

	public TurtleScreenView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public TurtleScreenView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	private void init(Context context) {
		mPaint = new Paint();
		// Note: Round cap does not work...
		// See Issue 24873 - android - Paint.setStrokeCap not working when using hardware acceleration
		mPaint.setStrokeCap(Paint.Cap.ROUND);
	}

	@Override
	protected void onDraw(@NotNull Canvas canvas) {
		if (mSensorColor != 0) {
			final int arrowRad = 24;
			final int turtleRad = 12;
			final Rect clipBounds = canvas.getClipBounds();

			int realSize = Math.min(clipBounds.width(), clipBounds.height());
			int offsetX = (clipBounds.width() - realSize) / 2;
			int offsetY = (clipBounds.height() - realSize) / 2;
			int size = realSize - turtleRad * 2;

			mPaint.setStyle(Paint.Style.FILL);
			mPaint.setColor(Color.DKGRAY);
			int left = offsetX + clipBounds.left;
			int top = offsetY + clipBounds.top;
			canvas.drawRect(left, top, left + realSize, top + realSize, mPaint);

			final float screenSize = mMaxPos - mMinPos;
			int x = turtleRad + offsetX + (int) ((mTurtleX - mMinPos) / screenSize * size);
			int y = turtleRad + offsetY + size - (int) ((mTurtleY - mMinPos) / screenSize * size);
			int stopX = x + (int) (arrowRad * Math.cos(mTheta));
			int stopY = y - (int) (arrowRad * Math.sin(mTheta));

			mPaint.setStrokeWidth(8);
			mPaint.setColor(Color.BLACK);
			canvas.drawLine(x, y, stopX, stopY, mPaint);
			// Note: Round cap does not work... so we draw a circle at the end.
			// See Issue 24873 - android - Paint.setStrokeCap not working when using hardware acceleration
			canvas.drawCircle(stopX, stopY, 4, mPaint);

			mPaint.setStyle(Paint.Style.FILL);
			mPaint.setColor(mSensorColor);
			canvas.drawCircle(x, y, turtleRad, mPaint);

			mPaint.setStrokeWidth(2);
			mPaint.setStyle(Paint.Style.STROKE);
			mPaint.setColor(Color.BLACK);
			canvas.drawCircle(x, y, turtleRad, mPaint);

			mPaint.setStrokeWidth(4);
			mPaint.setColor(mSensorColor);
			canvas.drawLine(x, y, stopX, stopY, mPaint);
		}
		super.onDraw(canvas);
	}

	public void setTurtleBorders(float min, float max) {
		mMinPos = min;
		mMaxPos = max;
	}

	public void setTurtleSensorColor(int color) {
		mSensorColor = color;
	}

	public void setTurtlePose(float x, float y, float theta) {
		mTurtleX = x;
		mTurtleY = y;
		mTheta = theta;
	}
}