/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.rca_plugin_turtlesim;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.github.rosjava.android_remctrlark.event_handling.IObservableRcaView;
import com.github.rosjava.android_remctrlark.event_handling.IRcaViewEventListener;
import com.google.common.base.Preconditions;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class TurtleChooser extends Spinner implements IObservableRcaView {

	private final List<String> mTurtles = new LinkedList<String>();
	private IRcaViewEventListener mCustomEventListener = null;
	private ArrayAdapter<String> mAdapter = null;

	public TurtleChooser(Context context) {
		super(context);
	}

	public TurtleChooser(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public TurtleChooser(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public void setCustomEventListener(IRcaViewEventListener listener) {
		Preconditions.checkState(getOnItemSelectedListener() == null);
		mCustomEventListener = listener;
		final OnItemSelectedListener wrapperListener;
		if (mCustomEventListener != null) {
			wrapperListener = new OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					dispatchCustomEvent("turtle_selected", getItemAtPosition(position));
				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					dispatchCustomEvent("nothing_selected", null);
				}
			};
		} else {
			wrapperListener = null;
		}
		super.setOnItemSelectedListener(wrapperListener);
	}

	public String getSelectedTurtle() {
		return (String) getSelectedItem();
	}

	public void setTurtles(JSONArray turtles) throws JSONException {
		setTurtles(turtles, null);
	}

	public void setTurtles(JSONArray turtles, String selectTurtle) throws JSONException {
		mTurtles.clear();
		for (int i = 0; i < turtles.length(); ++i) {
			mTurtles.add(turtles.getString(i));
		}
		updateTurtles(selectTurtle);
	}

	public void setTurtles(List<String> turtles) {
		setTurtles(turtles, null);
	}

	public void setTurtles(List<String> turtles, String selectTurtle) {
		mTurtles.clear();
		mTurtles.addAll(turtles);
		updateTurtles(selectTurtle);
	}

	public List<String> getTurtles() {
		return mTurtles;
	}

	private void dispatchCustomEvent(String name, Object data) {
		final IRcaViewEventListener rcaListener = mCustomEventListener;
		if (rcaListener != null) {
			rcaListener.onCustomEvent(this, name, data);
		}
	}

	private void updateTurtles(final String selectTurtle) {
		Collections.sort(mTurtles);
		post(new Runnable() {
			@Override
			public void run() {
				final String selectedTurtle = selectTurtle != null ? selectTurtle : (String) getSelectedItem();
				// Note: We always recreate the adapter in order to get a notification of new the selection. Otherwise
				// no notification is sent, e.g. when the position remains the same but the turtle name changes, or when
				// the list gets empty.
				if (mAdapter != null) {
					mAdapter.clear();
					mAdapter.notifyDataSetInvalidated();
				}
				if (!mTurtles.isEmpty()) {
					assert getContext() != null;
					mAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, new ArrayList<String>());
					mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					mAdapter.addAll(mTurtles);
					setAdapter(mAdapter);
					if (selectedTurtle != null) {
						int position = 0;
						for (String turtle : mTurtles) {
							if (turtle.equals(selectedTurtle)) {
								setSelection(position);
							}
							++position;
						}
					}
				}
			}
		});
	}
}
