/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.rca_plugin_turtlesim.rca_plugin_java.remocons;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Vibrator;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.TextView;

import com.github.rosjava.android_remctrlark.rcajava.binders.annotations.RcaType;
import com.github.rosjava.android_remctrlark.rcajava.configuration.IJavaRemoConConfiguration;
import com.github.rosjava.android_remctrlark.rcajava.context.IJavaRemoConContext;
import com.github.rosjava.android_remctrlark.rcajava.context.misc.IOnRemoCompLaunchedListener;
import com.github.rosjava.android_remctrlark.rcajava.context.misc.IRemoCompBinder;
import com.github.rosjava.android_remctrlark.rcajava.context.misc.IRemoCompEventsListener;
import com.github.rosjava.android_remctrlark.rcajava.context.misc.IRemoCompProxy;
import com.github.rosjava.android_remctrlark.rcajava.error_handling.RemoCompBinderInvocationException;
import com.github.rosjava.android_remctrlark.rcajava.misc.ICheckedRunnable;
import com.github.rosjava.android_remctrlark.rcajava_impl.AJavaRemoConImplBase;
import com.github.rosjava.android_remctrlark.rcajava_impl.error_handling.RosNodeError;
import com.github.rosjava.rca_plugin_turtlesim.ColorPickerView;
import com.github.rosjava.rca_plugin_turtlesim.TurtleChooser;
import com.github.rosjava.rca_plugin_turtlesim.rca_plugin_java.R;

import org.json.JSONException;
import org.json.JSONObject;
import org.ros.exception.RemoteException;
import org.ros.exception.ServiceNotFoundException;
import org.ros.internal.message.Message;
import org.ros.internal.message.RawMessage;
import org.ros.internal.node.client.MasterClient;
import org.ros.internal.node.response.Response;
import org.ros.master.client.SystemState;
import org.ros.master.client.TopicSystemState;
import org.ros.namespace.GraphName;
import org.ros.namespace.NameResolver;
import org.ros.node.AbstractNodeMain;
import org.ros.node.ConnectedNode;
import org.ros.node.Node;
import org.ros.node.NodeConfiguration;
import org.ros.node.NodeMain;
import org.ros.node.parameter.ParameterTree;
import org.ros.node.service.ServiceClient;
import org.ros.node.service.ServiceResponseListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@RcaType("This RemoCon is a Java implementation of an example remote control for ROS' turtlesim (http://wiki.ros.org/turtlesim).\n" +

		"\nNsIds:\n" +
		"\t" + TurtlesimTele.NSID_TURTLESIM + ": Used for addressing turtlesim's global services and parameters.\n" +
		"\t" + TurtlesimTele.NSID_TURTLE + ": Used for addressing turtlesim's turtle services and topics.\n" +

		"\nUsed graph names:\n" +
		"\t" + TurtlesimTele.NODE_NAME_TURTLESIM + ": The node name of the turtlesim node to connect to.\n" +

		"\nCalled services:\n" +
		"\t" + TurtlesimTele.CMD_TURTLESIM_CLEAR + " (std_srvs.Empty): turtlesim's global service 'clear'.\n" +
		"\t" + TurtlesimTele.CMD_TURTLESIM_RESET + " (std_srvs.Empty): turtlesim's global service 'reset'.\n" +
		"\t" + TurtlesimTele.CMD_TURTLESIM_SPAWN + " (turtlesim.Spawn): turtlesim's global service 'spawn'.\n" +
		"\t" + TurtlesimTele.CMD_TURTLE_KILL + " (turtlesim.Kill): turtlesim's global service 'kill'.\n" +
		"\t" + TurtlesimTele.CMD_TURTLE_SET_PEN + " (turtlesim.SetPen): turtlesim's turtle service 'set_pen'.\n" +
		"\t" + TurtlesimTele.CMD_TURTLE_TELEPORT_ABSOLUTE + " (turtlesim.TeleportAbsolute): turtlesim's turtle service 'teleport_absolute'.\n" +
		"\t" + TurtlesimTele.CMD_TURTLE_TELEPORT_RELATIVE + " (turtlesim.TeleportRelative): turtlesim's turtle service 'teleport_relative'.\n" +

		"\nServer parameters:\n" +
		"\t" + TurtlesimTele.SERVER_PARAM_BACKGROUND_R + " (int): Red color component for clearing turtlesim's background.\n" +
		"\t" + TurtlesimTele.SERVER_PARAM_BACKGROUND_G + " (int): Green color component for clearing turtlesim's background.\n" +
		"\t" + TurtlesimTele.SERVER_PARAM_BACKGROUND_B + " (int): Blue color component for clearing turtlesim's background.")
public class TurtlesimTele extends AJavaRemoConImplBase implements IRemoCompEventsListener, IOnRemoCompLaunchedListener {
	public static final String NODE_NAME_TURTLESIM = "turtlesim_node";

	public static final String NSID_TURTLESIM = "turtlesim";
	public static final String NSID_TURTLE = "turtle";

	public static final String REMOCOMP_JOYPAD = "joypad";
	public static final String REMOCOMP_TURTLESIM_COMMANDS = "turtlesim_commands";
	public static final String REMOCOMP_TURTLE_COMMANDS = "turtle_commands";
	public static final String REMOCOMP_TURTLE_SCREEN = "turtle_screen";

	public static final String CMD_TURTLESIM_SET_TURTLESIM = "set_turtlesim";
	public static final String CMD_TURTLESIM_UPDATE = "update";
	public static final String CMD_TURTLESIM_CLEAR = "clear";
	public static final String CMD_TURTLESIM_RESET = "reset";
	public static final String CMD_TURTLESIM_SPAWN = "spawn";
	public static final String CMD_TURTLE_SET_PEN = "set_pen";
	public static final String CMD_TURTLE_TELEPORT_ABSOLUTE = "teleport_absolute";
	public static final String CMD_TURTLE_TELEPORT_RELATIVE = "teleport_relative";
	public static final String CMD_TURTLE_KILL = "kill";

	public static final String SERVER_PARAM_BACKGROUND_R = "background_r";
	public static final String SERVER_PARAM_BACKGROUND_G = "background_g";
	public static final String SERVER_PARAM_BACKGROUND_B = "background_b";

	private static final float RAD = (float) (Math.PI / 180.0f);

	private class TurtlesimService {
		protected void prepareRequest(RawMessage request) {
		}

		protected void onSuccess(RawMessage response) {
		}

		public void call(final String name, String type) {
			final ConnectedNode connectedNode = mConnectedNode;
			if (connectedNode != null) {
				try {
					final ServiceClient<Message, Message> client = connectedNode.newServiceClient(name, type);
					final Message request = client.newMessage();
					prepareRequest(request.toRawMessage());
					client.call(request, new ServiceResponseListener<Message>() {
						@Override
						public void onSuccess(Message response) {
							TurtlesimService.this.onSuccess(response.toRawMessage());
							getLog().i("success calling service " + name);
						}

						@Override
						public void onFailure(RemoteException e) {
							getLog().e("failure calling service " + name);
						}
					});
				} catch (ServiceNotFoundException e) {
					getLog().e("service " + name + " not found");
				}
			}
		}

		protected String getTextValue(View content, int viewId, String defaultValue) {
			final TextView view = (TextView) content.findViewById(viewId);
			assert view != null;
			final CharSequence text = view.getText();
			final String value = text != null ? text.toString() : "";
			return value.length() > 0 ? value : defaultValue;
		}
	}

	private static class State implements Serializable {
		String mSelectedTurtle = null;
		int mPenColor = 0xffb3b8ff;
		int mPenWidth = 3;
		boolean mPenOff = false;
		float mTelRelLinear = 2f;
		float mTelRelAngular = 144f;
	}

	private State mState = null;
	private ConnectedNode mConnectedNode = null;
	private NodeMain mRosNode = null;
	private MasterClient mMasterClient = null;
	private NameResolver mResolver = null;
	private View mSwitchJoypad = null;
	private TurtleChooser mTurtleChooser = null;
	private Map<String, ICheckedRunnable> mCommands = new HashMap<String, ICheckedRunnable>();
	private GraphName mTurtlesimNodeName = null;
	private AlertDialog mDialog = null;

	@Override
	public void init(IJavaRemoConContext context) throws Throwable {
		super.init(context);
		mState = restoreRemoConSessionState(State.class);
		if (mState == null) {
			mState = new State();
			getRcaContext().setRemoConSessionState(mState);
		}

		mCommands.put(CMD_TURTLESIM_SET_TURTLESIM, new ICheckedRunnable() {
			@Override
			public void run() {
				if (mResolver == null) {
					return;
				}
				final Collection<TopicSystemState> state = getRosSystemState();
				if (state == null) {
					return;
				}

				final Set<String> turtlesimNodesSet = new HashSet<String>();
				for (TopicSystemState topic : state) {
					for (String publisher : topic.getPublishers()) {
						if (GraphName.of(publisher).getBasename().toString().equals(NSID_TURTLESIM)) {
							turtlesimNodesSet.add(mResolver.resolve(publisher).toString());
						}
					}
				}

				final String[] turtlesimNodes = turtlesimNodesSet.toArray(new String[turtlesimNodesSet.size()]);
				assert getParentView().getContext() != null;
				setDialog(new AlertDialog.Builder(getParentView().getContext())
						.setTitle("Choose Turtlesim")
						.setItems(turtlesimNodes, new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, final int which) {
								getRcaContext().postToImplThread(new ICheckedRunnable() {
									@Override
									public void run() {
										getRelauncher().setNamespace(NSID_TURTLESIM, GraphName.of(turtlesimNodes[which]).getParent().toString()).commit();
									}
								});
							}
						})
						.show());
			}
		});

		mCommands.put(CMD_TURTLESIM_UPDATE, new ICheckedRunnable() {
			@Override
			public void run() {
				updateTurtleChooser();
			}
		});

		mCommands.put(CMD_TURTLESIM_SPAWN, new ICheckedRunnable() {
			@Override
			public void run() {
				assert getParentView().getContext() != null;
				final View content = getLayout().getInflater().inflate(R.layout.turtle_spawn_dialog_content, null);
				assert content != null;
				setDialog(new AlertDialog.Builder(getParentView().getContext())
						.setTitle("Spawn Turtle")
						.setView(content)
						.setNegativeButton("Cancel", null)
						.setPositiveButton("Spawn", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								new TurtlesimService() {
									@Override
									protected void prepareRequest(RawMessage request) {
										request.setString("name", getTextValue(content, R.id.name, ""));
										request.setFloat32("x", Float.parseFloat(getTextValue(content, R.id.x, "5")));
										request.setFloat32("y", Float.parseFloat(getTextValue(content, R.id.y, "5")));
										request.setFloat32("theta", RAD * Float.parseFloat(getTextValue(content, R.id.theta, "0")));
									}

									@Override
									protected void onSuccess(RawMessage response) {
										updateTurtleChooser(response.getString("name"));
									}
								}.call(CMD_TURTLESIM_SPAWN, turtlesim.Spawn._TYPE);
							}
						})
						.show());
			}
		});

		mCommands.put(CMD_TURTLESIM_CLEAR, new ICheckedRunnable() {
			@Override
			public void run() {

				assert getParentView().getContext() != null;
				final View content = getLayout().getInflater().inflate(R.layout.turtlesim_clear_dialog_content, null);
				assert content != null;
				final ColorPickerView colorPicker = (ColorPickerView) content.findViewById(R.id.color);
				assert colorPicker != null;
				final int currentColor = getBackgroundParams();
				colorPicker.setDefaultColor(currentColor);
				colorPicker.setCurrentColor(currentColor);

				setDialog(new AlertDialog.Builder(getParentView().getContext())
						.setTitle("Clear")
						.setView(content)
						.setNegativeButton("Cancel", null)
						.setPositiveButton("Apply", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								getRcaContext().postToImplThread(new ICheckedRunnable() {
									@Override
									public void run() {
										setBackgroundParams(colorPicker.getCurrentColor());
										new TurtlesimService().call(CMD_TURTLESIM_CLEAR, std_srvs.Empty._TYPE);
									}
								});
							}
						})
						.show());
			}
		});

		mCommands.put(CMD_TURTLESIM_RESET, new ICheckedRunnable() {
			@Override
			public void run() {
				new TurtlesimService() {
					@Override
					protected void onSuccess(RawMessage response) {
						getRcaContext().runOnImplThread(new ICheckedRunnable() {
							@Override
							public void run() {
								// The turtlesim node will disconnect all subscribed topics, so we won't get any
								// messages anymore. As a workaround, reconnect all topics by forcing a remocon
								// relaunch.
								getRelauncher().setParam("relaunch_force", System.currentTimeMillis());
							}
						});
						updateTurtleChooser();
					}
				}.call(CMD_TURTLESIM_RESET, std_srvs.Empty._TYPE);
			}
		});

		mCommands.put(CMD_TURTLE_SET_PEN, new ICheckedRunnable() {
			@Override
			public void run() {
				final GraphName turtle = getTurtleNs();
				if (turtle == null) {
					return;
				}

				assert getParentView().getContext() != null;
				final View content = getLayout().getInflater().inflate(R.layout.turtle_set_pen_dialog_content, null);
				assert content != null;

				final ColorPickerView colorPicker = (ColorPickerView) content.findViewById(R.id.color);
				assert colorPicker != null;
				colorPicker.setDefaultColor(mState.mPenColor);
				colorPicker.setCurrentColor(mState.mPenColor);

				final SeekBar width = (SeekBar) content.findViewById(R.id.width);
				width.setProgress(mState.mPenWidth);

				final CheckBox off = (CheckBox) content.findViewById(R.id.off);
				off.setChecked(mState.mPenOff);

				setDialog(new AlertDialog.Builder(getParentView().getContext())
						.setTitle("Set Pen")
						.setView(content)
						.setNegativeButton("Cancel", null)
						.setPositiveButton("Apply", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								getRcaContext().postToImplThread(new ICheckedRunnable() {
									@Override
									public void run() {
										new TurtlesimService() {
											@Override
											protected void prepareRequest(RawMessage request) {
												mState.mPenColor = colorPicker.getCurrentColor();
												request.setUInt8("r", (byte) Color.red(mState.mPenColor));
												request.setUInt8("g", (byte) Color.green(mState.mPenColor));
												request.setUInt8("b", (byte) Color.blue(mState.mPenColor));
												request.setUInt8("width", (byte) (mState.mPenWidth = width.getProgress()));
												request.setUInt8("off", (byte) ((mState.mPenOff = off.isChecked()) ? 1 : 0));
											}
										}.call(turtle.join(CMD_TURTLE_SET_PEN).toString(), turtlesim.SetPen._TYPE);
									}
								});
							}
						})
						.show());
			}
		});

		mCommands.put(CMD_TURTLE_TELEPORT_ABSOLUTE, new ICheckedRunnable() {
			@Override
			public void run() {
				final GraphName turtleNs = getTurtleNs();
				if (turtleNs == null) {
					return;
				}

				assert getParentView().getContext() != null;
				final View content = getLayout().getInflater().inflate(R.layout.turtle_teleport_absolute_dialog_content, null);
				assert content != null;
				setDialog(new AlertDialog.Builder(getParentView().getContext())
						.setTitle("Teleport Turtle")
						.setView(content)
						.setNegativeButton("Cancel", null)
						.setPositiveButton("Teleport", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								new TurtlesimService() {
									@Override
									protected void prepareRequest(RawMessage request) {
										request.setFloat32("x", Float.parseFloat(getTextValue(content, R.id.x, "5")));
										request.setFloat32("y", Float.parseFloat(getTextValue(content, R.id.y, "5")));
										request.setFloat32("theta", Float.parseFloat(getTextValue(content, R.id.theta, "0")));
									}
								}.call(turtleNs.join(CMD_TURTLE_TELEPORT_ABSOLUTE).toString(), turtlesim.TeleportAbsolute._TYPE);
							}
						})
						.show());
			}
		});

		mCommands.put(CMD_TURTLE_TELEPORT_RELATIVE, new ICheckedRunnable() {
			@Override
			public void run() {
				final GraphName turtleNs = getTurtleNs();
				if (turtleNs == null) {
					return;
				}

				assert getParentView().getContext() != null;
				final View content = getLayout().getInflater().inflate(R.layout.turtle_teleport_relative_dialog, null);
				assert content != null;

				((TextView) content.findViewById(R.id.linear)).setText(Float.toString(mState.mTelRelLinear));
				((TextView) content.findViewById(R.id.angular)).setText(Float.toString(mState.mTelRelAngular));

				final AlertDialog dialog = new AlertDialog.Builder(getParentView().getContext())
						.setTitle("Teleport Turtle")
						.setView(content)
						.show();
				setDialog(dialog);

				content.findViewById(R.id.apply).setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						new TurtlesimService() {
							@Override
							protected void prepareRequest(RawMessage request) {
								mState.mTelRelLinear = Float.parseFloat(getTextValue(content, R.id.linear, "1"));
								mState.mTelRelAngular = Float.parseFloat(getTextValue(content, R.id.angular, "42"));
								request.setFloat32("linear", mState.mTelRelLinear);
								request.setFloat32("angular", RAD * mState.mTelRelAngular);
							}
						}.call(turtleNs.join(CMD_TURTLE_TELEPORT_RELATIVE).toString(), turtlesim.TeleportRelative._TYPE);
					}
				});

				content.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();
					}
				});
			}
		});

		mCommands.put(CMD_TURTLE_KILL, new ICheckedRunnable() {
			@Override
			public void run() {
				final String turtle = mTurtleChooser.getSelectedTurtle();
				new TurtlesimService() {
					@Override
					protected void onSuccess(RawMessage response) {
						updateTurtleChooser();
					}

					@Override
					protected void prepareRequest(RawMessage request) {
						request.setString("name", turtle);
					}
				}.call(CMD_TURTLE_KILL, turtlesim.Kill._TYPE);
			}
		});
	}

	@Override
	public void createUI() {
		getLayout().getInflater().inflate(R.layout.turtlesim, getParentView());
	}

	@Override
	public void start(IJavaRemoConConfiguration configuration) {
		mRosNode = new AbstractNodeMain() {
			@Override
			public GraphName getDefaultNodeName() {
				return GraphName.of(getRcaContext().getType());
			}

			@Override
			public void onStart(ConnectedNode connectedNode) {
				mConnectedNode = connectedNode;
			}

			@Override
			public void onShutdown(Node node) {
				mConnectedNode = null;
			}

			@Override
			public void onError(final Node node, final Throwable throwable) {
				getRcaContext().handleException(new RosNodeError(node, throwable));
			}
		};

		final NodeConfiguration nodeConfiguration = configuration.createNodeConfiguration(getRcaContext().getType());
		mResolver = nodeConfiguration.getParentResolver();
		mTurtlesimNodeName = mResolver.resolve(NODE_NAME_TURTLESIM);
		mMasterClient = new MasterClient(nodeConfiguration.getMasterUri());

		getRcaContext().getNodeExecutor().execute(mRosNode, nodeConfiguration);

		getRcaContext().getRemoCompProxy(REMOCOMP_JOYPAD).setOnLaunchedListener(this);
		getRcaContext().getRemoCompProxy(REMOCOMP_TURTLESIM_COMMANDS).setEventsListener(this);
		getRcaContext().getRemoCompProxy(REMOCOMP_TURTLE_COMMANDS).setEventsListener(this);
		getRcaContext().getRemoCompProxy(REMOCOMP_TURTLE_SCREEN).setEventsListener(this);

		mTurtleChooser = (TurtleChooser) getParentView().findViewById(R.id.turtle_chooser);

		updateTurtleChooser(mState.mSelectedTurtle);

		mTurtleChooser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, final int position, long id) {
				enableTurtleUi();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				disableTurtleUi();
			}
		});

		mSwitchJoypad = getParentView().findViewById(R.id.button_switch_joypad);
		mSwitchJoypad.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getRcaContext().postToImplThread(new ICheckedRunnable() {
					@Override
					public void run() throws Throwable {
						final IRemoCompProxy proxy = getRcaContext().getRemoCompProxy(REMOCOMP_JOYPAD);
						if (proxy.getType().equals("simple_joypad")) {
							getRelauncher().setType(REMOCOMP_JOYPAD, "ros_virtual_joystick", "com.github.rosjava.rca_plugin_std");
						} else {
							getRelauncher().setType(REMOCOMP_JOYPAD, "simple_joypad", "com.github.rosjava.rca_plugin_std");
						}
						getRelauncher().commit();
					}
				});
			}
		});
	}

	@Override
	public void shutdown() {
		setDialog(null);

		if (mRosNode != null) {
			getRcaContext().getNodeExecutor().shutdownNodeMain(mRosNode);
			mConnectedNode = null;
			mMasterClient = null;
			mRosNode = null;
		}

		if (mTurtleChooser != null) {
			mState.mSelectedTurtle = mTurtleChooser.getSelectedTurtle();
			mTurtleChooser.setOnItemSelectedListener(null);
			mTurtleChooser = null;
		}

		if (mSwitchJoypad != null) {
			mSwitchJoypad.setOnClickListener(null);
			mSwitchJoypad = null;
		}
	}

	@Override
	public void onRemoCompLaunched(IRemoCompProxy proxy) throws Throwable {
		if (proxy.getType().equals("ros_virtual_joystick")) {
			final View joypad = getParentView().findViewById(R.id.joypad);
			assert joypad.getLayoutParams() != null;
			//noinspection SuspiciousNameCombination
			joypad.getLayoutParams().width = joypad.getLayoutParams().height;
			getRcaContext().postToUiThread(new ICheckedRunnable() {
				@Override
				public void run() throws Throwable {
					joypad.requestLayout();
				}
			});
		}
	}

	@Override
	public void onRemoCompEvent(IRemoCompProxy proxy, String name, Object data) throws JSONException {
		if (name.equals("hit_the_wall")) {
			final Vibrator vibrator = (Vibrator) getApkContext().getSystemService(Context.VIBRATOR_SERVICE);
			assert vibrator != null;
			vibrator.vibrate(100);
		} else if (name.equals("buttons_container_event")) {
			final JSONObject jsonObject = (JSONObject) data;
			final String message = jsonObject.getString("message");
			callCommand(message);
		} else {
			getLog().w("ignoring unknown remocomp event: '" + name + "'");
		}
	}

	private void updateTurtleChooser() {
		updateTurtleChooser(null);
	}

	private void updateTurtleChooser(String selectTurtle) {
		final List<String> turtleTopics = new ArrayList<String>(3);
		turtleTopics.add("cmd_vel");
		turtleTopics.add("color_sensor");
		turtleTopics.add("pose");
		final Map<GraphName, Integer> turtleCandidates = new HashMap<GraphName, Integer>();

		final Collection<TopicSystemState> state = getRosSystemState();
		final GraphName turltesimNs = mTurtlesimNodeName.getParent();
		final String turtlesimNodeName = mTurtlesimNodeName.toString();
		for (TopicSystemState topic : state) {
			final GraphName topicGraphName = GraphName.of(topic.getTopicName());
			if (turtleTopics.contains(topicGraphName.getBasename().toString()) &&
					topicGraphName.getParent().getParent().equals(turltesimNs) &&
					(topic.getPublishers().contains(turtlesimNodeName) || topic.getSubscribers().contains(turtlesimNodeName))) {
				final GraphName turtleNamespace = topicGraphName.getParent();
				final Integer matchedTopicsCount = turtleCandidates.get(turtleNamespace);
				turtleCandidates.put(turtleNamespace, (matchedTopicsCount != null ? matchedTopicsCount : 0) + 1);
			}
		}

		final List<String> turtles = new LinkedList<String>();
		for (Map.Entry<GraphName, Integer> entry : turtleCandidates.entrySet()) {
			if (entry.getValue() == turtleTopics.size()) {
				turtles.add(entry.getKey().getBasename().toString());
			}
		}

		if (turtles.isEmpty()) {
			disableTurtleUi();
		}
		mTurtleChooser.setTurtles(turtles, selectTurtle);
	}

	private void enableTurtleUi() {
		getRcaContext().runOnUiThread(new ICheckedRunnable() {
			@Override
			public void run() {
				mTurtleChooser.setEnabled(true);
			}
		});
		getRcaContext().runOnImplThread(new ICheckedRunnable() {
			@Override
			public void run() throws RemoCompBinderInvocationException {
				getRcaContext().getRemoCompProxy(REMOCOMP_TURTLE_COMMANDS).getBinder().invokeMethod("setAllButtonsEnabled", true);
				getRcaContext().getRemoCompProxy(REMOCOMP_TURTLESIM_COMMANDS).getBinder().invokeMethod("setAllButtonsEnabled", true);
				getRelauncher().setNamespace(NSID_TURTLE, getTurtleNs()).commit();
			}
		});
	}

	private void disableTurtleUi() {
		getRcaContext().runOnUiThread(new ICheckedRunnable() {
			@Override
			public void run() {
				if (mTurtleChooser != null) {
					mTurtleChooser.setEnabled(false);
				}
			}
		});
		getRcaContext().runOnImplThread(new ICheckedRunnable() {
			@Override
			public void run() throws RemoCompBinderInvocationException {
				getRcaContext().getRemoCompProxy(REMOCOMP_TURTLE_COMMANDS).getBinder().invokeMethod("setAllButtonsEnabled", false);
				final IRemoCompBinder binder = getRcaContext().getRemoCompProxy(REMOCOMP_TURTLESIM_COMMANDS).getBinder();
				binder.invokeMethod("setAllButtonsEnabled", false);
				binder.invokeMethod("setButtonEnabled", CMD_TURTLESIM_SET_TURTLESIM, true);
				binder.invokeMethod("setButtonEnabled", CMD_TURTLESIM_UPDATE, true);
				binder.invokeMethod("setButtonEnabled", CMD_TURTLESIM_SPAWN, true);

				getRelauncher().setNamespace(NSID_TURTLE, "").commit();
			}
		});
	}

	private void callCommand(final String cmdName) {
		getRcaContext().runOnImplThread(new ICheckedRunnable() {
			@Override
			public void run() throws Throwable {
				final ICheckedRunnable command = mCommands.get(cmdName);
				if (command != null) {
					command.run();
				} else {
					getLog().w("ignoring unknown command: '" + cmdName + "'");
				}
			}
		});
	}

	private GraphName getTurtleNs() {
		final String selectedTurtle = mTurtleChooser.getSelectedTurtle();
		if (selectedTurtle == null) {
			return null;
		}
		return mTurtlesimNodeName.getParent().join(selectedTurtle);
	}

	private int getBackgroundParams() {
		assert mConnectedNode != null;
		final ParameterTree parameterTree = mConnectedNode.getParameterTree();
		return Color.rgb(
				parameterTree.getInteger(SERVER_PARAM_BACKGROUND_R, 0),
				parameterTree.getInteger(SERVER_PARAM_BACKGROUND_G, 0),
				parameterTree.getInteger(SERVER_PARAM_BACKGROUND_B, 0));
	}

	private void setBackgroundParams(int color) {
		assert mConnectedNode != null;
		final ParameterTree parameterTree = mConnectedNode.getParameterTree();
		parameterTree.set(SERVER_PARAM_BACKGROUND_R, Color.red(color));
		parameterTree.set(SERVER_PARAM_BACKGROUND_G, Color.green(color));
		parameterTree.set(SERVER_PARAM_BACKGROUND_B, Color.blue(color));
	}

	private Collection<TopicSystemState> getRosSystemState() {
		Throwable error = null;
		try {
			final Response<SystemState> systemState = mMasterClient.getSystemState(GraphName.empty());
			if (systemState.isSuccess()) {
				return systemState.getResult().getTopics();
			}
		} catch (Throwable e) {
			error = e;
		}
		throw new RuntimeException("Could not retrieve ROS system state.", error);
	}

	private void setDialog(AlertDialog dialog) {
		if (mDialog != null && mDialog.isShowing()) {
			mDialog.dismiss();
		}
		mDialog = dialog;
	}
}
