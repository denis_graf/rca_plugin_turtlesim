/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.rca_plugin_turtlesim.rca_plugin_java.remocomps;

import android.graphics.Color;

import com.github.rosjava.android_remctrlark.rcajava.configuration.IJavaRemoCompConfiguration;
import com.github.rosjava.android_remctrlark.rcajava.error_handling.RcaLayoutException;
import com.github.rosjava.android_remctrlark.rcajava.misc.ICheckedRunnable;
import com.github.rosjava.android_remctrlark.rcajava_impl.AJavaRemoCompImplBase;
import com.github.rosjava.rca_plugin_turtlesim.TurtleScreenView;
import com.github.rosjava.rca_plugin_turtlesim.rca_plugin_java.R;

import org.json.JSONException;
import org.ros.android.message_processor.AsyncMessageProcessorNodeMain;
import org.ros.android.message_processor.MessageProcessor;
import org.ros.android.message_processor.MessageProcessorNodeMain;
import org.ros.internal.message.RawMessage;
import org.ros.internal.node.NodeMainGroup;
import org.ros.namespace.GraphName;
import org.ros.node.NodeConfiguration;

import java.io.StringWriter;
import java.text.DecimalFormat;

public class TurtleScreen extends AJavaRemoCompImplBase {

	public static final String TOPIC_NAME_POSE = "pose";
	public static final String TOPIC_NAME_COLOR_SENSOR = "color_sensor";

	private NodeMainGroup mRosNode = null;
	private MessageProcessorNodeMain<RawMessage> mPoseRosNode = null;
	private MessageProcessorNodeMain<RawMessage> mColorSensorRosNode = null;
	private TurtleScreenView mView = null;
	private float mLastX = Float.NaN;
	private float mLastY = Float.NaN;
	private float mMinPos = 0f;
	private float mMaxPos = 11.08f;
	private String mTurtleNs = null;

	@Override
	public void createUI() throws RcaLayoutException {
		getLayout().getInflater().inflate(R.layout.turtle_screen, getParentView());
		mView = (TurtleScreenView) getParentView().findViewById(R.id.info_text);
		mMinPos = (float) getConfig().getParamDouble("min_pos", mMinPos);
		mMaxPos = (float) getConfig().getParamDouble("max_pos", mMaxPos);
		mView.setTurtleBorders(mMinPos, mMaxPos);
	}

	@Override
	public void start(IJavaRemoCompConfiguration configuration) {
		final GraphName defaultNodeName = GraphName.of(getRcaContext().getType());

		mPoseRosNode = new AsyncMessageProcessorNodeMain<RawMessage, RawMessage>(turtlesim.Pose._TYPE, TOPIC_NAME_POSE, defaultNodeName);
		mPoseRosNode.setMessageProcessor(new MessageProcessor<RawMessage>() {
			@Override
			public void processMessage(RawMessage message) {

				final float x = message.getFloat32("x");
				final float y = message.getFloat32("y");
				final float theta = message.getFloat32("theta");

				if ((x != mLastX || y != mLastY) && (mMinPos >= x || x >= mMaxPos || mMinPos >= y || y >= mMaxPos)) {
					mLastX = x;
					mLastY = y;
					try {
						getEventDispatcher().dispatch("hit_the_wall", null);
					} catch (JSONException e) {
						getRcaContext().handleException(e);
					}
				}

				final DecimalFormat f = new DecimalFormat("#.##");
				final StringWriter out = new StringWriter();
				out.append("Connected turtle:\n")
						.append("\t").append(mTurtleNs).append("\n")
						.append("Pose:\n")
						.append("\tx: ").append(f.format(x)).append("\n")
						.append("\ty: ").append(f.format(y)).append("\n")
						.append("\ttheta: ").append(f.format(theta)).append("\n")
						.append("Velocity:\n")
						.append("\tlinear: ").append(f.format(message.getFloat32("linear_velocity"))).append("\n")
						.append("\tangular: ").append(f.format(message.getFloat32("angular_velocity"))).append("\n");

				final String finalMessage = out.toString();

				getRcaContext().syncRunOnUiThread(new ICheckedRunnable() {
					@Override
					public void run() {
						// Note: {@code mView} can be set to null from other threads.
						final TurtleScreenView view = mView;
						if (view != null) {
							view.setText(finalMessage);
							view.setTurtlePose(x, y, theta);
							view.invalidate();
						}
					}
				});
			}
		});

		mColorSensorRosNode = new AsyncMessageProcessorNodeMain<RawMessage, RawMessage>(turtlesim.Color._TYPE, TOPIC_NAME_COLOR_SENSOR, defaultNodeName);
		mColorSensorRosNode.setMessageProcessor(new MessageProcessor<RawMessage>() {
			@Override
			public void processMessage(RawMessage message) {
				// Note: Unfortunately we can't use getUInt8(), because it's not correctly implemented. So we have to
				// convert to unsigned byte by our own.
				final int color = Color.rgb(
						ubyte(message.getInt8("r")),
						ubyte(message.getInt8("g")),
						ubyte(message.getInt8("b")));
				final TurtleScreenView view = mView;
				getRcaContext().syncRunOnUiThread(new ICheckedRunnable() {
					@Override
					public void run() {
						if (view != null) {
							view.setTurtleSensorColor(color);
							view.invalidate();
						}
					}
				});
			}
		});

		mRosNode = new NodeMainGroup(defaultNodeName);
		mRosNode.addNodeMain(mPoseRosNode);
		mRosNode.addNodeMain(mColorSensorRosNode);

		final NodeConfiguration nodeConfiguration = configuration.createNodeConfiguration(defaultNodeName);
		mTurtleNs = nodeConfiguration.getParentResolver().resolve(TOPIC_NAME_POSE).getParent().toString();
		getNodeExecutor().execute(mRosNode, nodeConfiguration);
	}

	@Override
	public void shutdown() {
		if (mRosNode != null) {
			getNodeExecutor().shutdownNodeMain(mRosNode);
			mView = null;
			mRosNode = null;
			mPoseRosNode = null;
			mColorSensorRosNode = null;
		}
	}

	private static int ubyte(byte b) {
		return b & 0xff;
	}
}
