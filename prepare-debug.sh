#!/bin/bash

# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

SCRIPT_DIR=$(dirname $0)
. ${SCRIPT_DIR}/scripts-config.sh

echo "adb forward tcp:${PYTHON_AP_PORT_LOCAL} tcp:${PYTHON_AP_PORT_REMOTE}"
if ! $RUN_ADB forward tcp:${PYTHON_AP_PORT_LOCAL} tcp:${PYTHON_AP_PORT_REMOTE} ; then
  exit 1
fi

echo "adb shell setprop debug.assert 1"
if ! $RUN_ADB shell setprop debug.assert 1 ; then
  exit 1
fi
